# Microservice API

Ce microservice est le point d'entrée de l'API.

Il est designé comme un binaire monolithique car il ne supporte qu'une route.
Toutefois, si l'API venait à grossir, il devrait être tourné comme une passerelle
pour parler à d'autres microservices (via des messages JSON, NATS ou gRPC).

Le microservice est basé sur Gin-Gonic/Gin et urfave/cli

# Routes

S'en suit les routes exposées et leur documentation.

## Weather

### GET /api/weather/:city

Récupère la météo à propos d'une ville, comme le taux de nuages, les coordonnées,
la pression, les niveaux de pluie et de neige...

La réponse est présentée par la structure Go suivante.

```go
type Response struct {
	Clouds struct {
	    All float64 `json:"all"`
    } `json:"clouds"`
	Coord struct {
        Lon float64 `json:"lon"`
        Lat float64 `json:"lat"`
    } `json:"coord"`
	Main struct {
        Temp        float64 `json:"temp"`
        FeelsLike   float64 `json:"feels_like"`
        Pressure    int     `json:"pressure"`
        Humidity    int     `json:"humidity"`
        TempMin     float64 `json:"temp_min"`
        TempMax     float64 `json:"temp_max"`
        SeaLevel    *int    `json:"sea_level,omitempty"`
        GroundLevel *int    `json:"grnd_level,omitempty"`
    } `json:"main"`
	Name string `json:"name"`
	Rain *struct {
        OneHour   float64 `json:"one_h"`
        ThreeHour float64 `json:"three_h"`
    } `json:"rain,omitempty"`
	Snow *struct {
        OneHour   float64 `json:"one_h"`
        ThreeHour float64 `json:"three_h"`
    } `json:"snow,omitempty"`
	Sys struct {
        Sunrise int `json:"sunrise"`
        Sunset  int `json:"sunset"`
    } `json:"sys"`
	Weather struct {
        ID          int    `json:"id"`
        Main        string `json:"main"`
        Description string `json:"description"`
        Icon        string `json:"icon"`
    } `json:"weather"`
	Wind struct {
        Speed float64  `json:"speed"`
        Deg   int      `json:"deg"`
        Gust  *float64 `json:"gust,omitempty"`
    } `json:"wind"`
}
```
