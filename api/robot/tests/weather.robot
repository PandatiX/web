*** Settings ***
Documentation  Test suite for /api/weather routes.
Library        REST    http://192.168.49.2


*** Test Cases ***
GET Weather Brest
    [Documentation]  This test ensures the REST API works for a well-known
    ...              station.
    [Teardown]       Output  response body

    GET         api/weather/Brest
    Integer     response status          200
    Number      response body coord lon  23.7
    Number      response body coord lat  52.1

GET Weather City Not Found
    [Documentation]  This test ensures the REST API fails in case of an unknown
    ...              station.
    [Teardown]       Output  response body

    GET         api/weather/idontexists
    Integer     response status          404
