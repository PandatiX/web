package main

import (
	"fmt"
	"os"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/urfave/cli/v2"
	"gitlab.com/PandatiX/web/api/internal"
	"gitlab.com/PandatiX/web/api/internal/weather"
	"go.uber.org/zap"
)

const cliLogo = `    ___    ____  ____
   /   |  / __ \/  _/
  / /| | / /_/ // /
 / ___ |/ ____// /
/_/  |_/_/   /___/

`

func main() {
	fmt.Print(cliLogo)

	app := &cli.App{
		Name:    "API",
		Usage:   "CLI to manage the API binary",
		Action:  startServer,
		Version: "v0.1.0",
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:        "owm-api-key",
				Aliases:     []string{"k"},
				Usage:       "OpenWeatherMap API key",
				EnvVars:     []string{"OPEN_WEATHER_MAP_API_KEY"},
				Destination: &weather.Key,
			},
			&cli.StringFlag{
				Name:        "units",
				Aliases:     []string{"u"},
				Usage:       "define the units to use (standard, metric, imperial)",
				Value:       "metric",
				Destination: &weather.Units,
			},
			&cli.StringFlag{
				Name:        "lang",
				Aliases:     []string{"l"},
				Usage:       "defines the language to use",
				Value:       "fr",
				Destination: &weather.Lang,
			},
			&cli.StringFlag{
				Name:    "gin-mode",
				Aliases: []string{"m"},
				Usage:   "defines the mode Gin logs",
				EnvVars: []string{"GIN_MODE"},
				Value:   gin.ReleaseMode,
			},
			&cli.IntFlag{
				Name:    "port",
				Aliases: []string{"p"},
				Usage:   "port to serves on",
				EnvVars: []string{"PORT"},
				Value:   80,
			},
		},
	}

	err := app.Run(os.Args)
	if err != nil {
		panic(err)
	}
}

func startServer(c *cli.Context) error {
	gin.SetMode(c.String("gin-mode"))

	router := gin.Default()
	api := router.Group("/api")
	{
		api.GET("/weather/:city", weather.WeatherGETHandler)
	}

	port := c.Int("port")
	internal.GetLogger().Info("Gin server serving", zap.Int("port", port))
	return router.Run(":" + strconv.Itoa(port))
}
