package internal

import (
	"sync"

	"go.uber.org/zap"
)

var onceLogger sync.Once
var logger *zap.Logger

func GetLogger() *zap.Logger {
	onceLogger.Do(func() {
		logger, _ = zap.NewProduction()
	})
	return logger
}
