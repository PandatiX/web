package weather

import (
	"encoding/json"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

var (
	Key   string
	Units string
	Lang  string
)

// ErrUnexpectedCod est une erreur signifiant qu'OWM a
// retourné un champ Cod non attendu dans la réponse JSON.
type ErrUnexpectedCod struct {
	Cod int
}

func (e ErrUnexpectedCod) Error() string {
	return "unexpected cod (" + strconv.Itoa(e.Cod) + ")"
}

func WeatherGETHandler(c *gin.Context) {
	// Récupère les paramètres
	city := c.Param("city")

	b, err := FetchOWM(DefaultClient, city)
	if err != nil {
		c.AbortWithError(http.StatusInternalServerError, err)
		return
	}

	// Unmarshal
	var wr OpenWeatherMapResponse
	err = json.Unmarshal(b, &wr)
	if err != nil {
		// On reçoit une erreur lors de la lecture de la réponse de l'API
		// dans la version actuelle car le champ Cod est de type int si la requête est
		// valide, et string sinon.
		// Cela est une erreur de conception de leur API !
		c.AbortWithError(http.StatusNotFound, err)
		return
	}

	if wr.Cod != http.StatusOK {
		c.AbortWithError(http.StatusInternalServerError, &ErrUnexpectedCod{wr.Cod})
		return
	}

	c.JSON(http.StatusOK, OWMRToResponse(wr))
}

type OpenWeatherMapResponse struct {
	Coord      Coord     `json:"coord"`
	Weather    []Weather `json:"weather"`
	Base       string    `json:"base"`
	Main       Main      `json:"main"`
	Visibility int       `json:"visibility"` // TODO trouver d'où vient ce champ (n'est pas dans la doc de l'API)
	Wind       Wind      `json:"wind"`
	Clouds     Clouds    `json:"clouds"`
	Rain       *OWPHours `json:"rain"`
	Snow       *OWPHours `json:"snow"`
	Dt         int       `json:"dt"`
	Sys        OWPSys    `json:"sys"`
	Timezone   int       `json:"timezone"`
	ID         int       `json:"id"`
	Name       string    `json:"name"`
	Cod        int       `json:"cod"`
}

type WeatherResponse struct {
	Clouds  Clouds  `json:"clouds"`
	Coord   Coord   `json:"coord"`
	Main    Main    `json:"main"`
	Name    string  `json:"name"`
	Rain    *Hours  `json:"rain,omitempty"`
	Snow    *Hours  `json:"snow,omitempty"`
	Sys     Sys     `json:"sys"`
	Weather Weather `json:"weather"`
	Wind    Wind    `json:"wind"`
}

func OWMRToResponse(wr OpenWeatherMapResponse) WeatherResponse {
	var w Weather
	if len(wr.Weather) != 0 {
		w = wr.Weather[0]
	}

	return WeatherResponse{
		Clouds: wr.Clouds,
		Coord:  wr.Coord,
		Main:   wr.Main,
		Rain:   OWPHToH(wr.Rain),
		Snow:   OWPHToH(wr.Snow),
		Sys: Sys{
			Sunrise: wr.Sys.Sunrise,
			Sunset:  wr.Sys.Sunset,
		},
		Weather: w,
		Wind:    wr.Wind,
	}
}

func OWPHToH(oh *OWPHours) *Hours {
	if oh == nil {
		return nil
	}
	return &Hours{
		OneHour:   oh.OneHour,
		ThreeHour: oh.ThreeHour,
	}
}

type OWPSys struct {
	Type    int     `json:"type"`
	ID      int     `json:"id"`
	Message *string `json:"message,omitempty"`
	Country string  `json:"country"`
	Sunrise int     `json:"sunrise"`
	Sunset  int     `json:"sunset"`
}

type Clouds struct {
	All float64 `json:"all"`
}

type Coord struct {
	Lon float64 `json:"lon"`
	Lat float64 `json:"lat"`
}

type Main struct {
	Temp        float64 `json:"temp"`
	FeelsLike   float64 `json:"feels_like"`
	Pressure    int     `json:"pressure"`
	Humidity    int     `json:"humidity"`
	TempMin     float64 `json:"temp_min"`
	TempMax     float64 `json:"temp_max"`
	SeaLevel    *int    `json:"sea_level,omitempty"`
	GroundLevel *int    `json:"grnd_level,omitempty"`
}

type OWPHours struct {
	OneHour   float64 `json:"1h"`
	ThreeHour float64 `json:"3h"`
}

type Hours struct {
	OneHour   float64 `json:"one_h"`
	ThreeHour float64 `json:"three_h"`
}

type Sys struct {
	Sunrise int `json:"sunrise"`
	Sunset  int `json:"sunset"`
}

type Weather struct {
	ID          int    `json:"id"`
	Main        string `json:"main"`
	Description string `json:"description"`
	Icon        string `json:"icon"`
}

type Wind struct {
	Speed float64  `json:"speed"`
	Deg   int      `json:"deg"`
	Gust  *float64 `json:"gust,omitempty"`
}
