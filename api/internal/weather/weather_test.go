package weather_test

import (
	"reflect"
	"testing"

	"gitlab.com/PandatiX/web/api/internal/weather"
)

func TestOWMRToResponse(t *testing.T) {
	t.Parallel()

	var tests = map[string]struct {
		WR               weather.OpenWeatherMapResponse
		ExpectedResponse weather.WeatherResponse
	}{
		"empty": {
			WR:               weather.OpenWeatherMapResponse{},
			ExpectedResponse: weather.WeatherResponse{},
		},
		"brest-sample": {
			WR: weather.OpenWeatherMapResponse{
				Coord: weather.Coord{
					Lon: -4.4833,
					Lat: 48.4,
				},
				Weather: []weather.Weather{
					{
						ID:          804,
						Main:        "Clouds",
						Description: "couvert",
						Icon:        "04d",
					},
				},
				Base: "stations",
				Main: weather.Main{
					Temp:      12.28,
					FeelsLike: 11.55,
					TempMin:   12,
					TempMax:   13,
					Pressure:  1005,
					Humidity:  76,
				},
				Visibility: 10000,
				Wind: weather.Wind{
					Speed: 7.2,
					Deg:   250,
				},
				Clouds: weather.Clouds{
					All: 90,
				},
				Dt: 1620301398,
				Sys: weather.OWPSys{
					Type:    1,
					ID:      6554,
					Country: "FR",
					Sunrise: 1620276629,
					Sunset:  1620329913,
				},
				Timezone: 7200,
				ID:       3030300,
				Name:     "Brest",
				Cod:      200,
			},
			ExpectedResponse: weather.WeatherResponse{
				Clouds: weather.Clouds{
					All: 90,
				},
				Coord: weather.Coord{
					Lon: -4.4833,
					Lat: 48.4,
				},
				Main: weather.Main{
					Temp:      12.28,
					FeelsLike: 11.55,
					TempMin:   12,
					TempMax:   13,
					Pressure:  1005,
					Humidity:  76,
				},
				Rain: nil,
				Snow: nil,
				Sys: weather.Sys{
					Sunrise: 1620276629,
					Sunset:  1620329913,
				},
				Weather: weather.Weather{
					ID:          804,
					Main:        "Clouds",
					Description: "couvert",
					Icon:        "04d",
				},
				Wind: weather.Wind{
					Speed: 7.2,
					Deg:   250,
				},
			},
		},
	}

	for testname, tt := range tests {
		t.Run(testname, func(t *testing.T) {
			r := weather.OWMRToResponse(tt.WR)

			if !reflect.DeepEqual(r, tt.ExpectedResponse) {
				t.Errorf("Failed to get expected weather result: got \"%v\" instead of \"%v\".", r, tt.ExpectedResponse)
			}
		})
	}
}

func TestOWPHToH(t *testing.T) {
	t.Parallel()

	var tests = map[string]struct {
		OWPHours      *weather.OWPHours
		ExpectedHours *weather.Hours
	}{
		"nil": {
			OWPHours:      nil,
			ExpectedHours: nil,
		},
		"empty": {
			OWPHours:      &weather.OWPHours{},
			ExpectedHours: &weather.Hours{},
		},
		"not-empty": {
			OWPHours: &weather.OWPHours{
				OneHour:   1.0,
				ThreeHour: 3.0,
			},
			ExpectedHours: &weather.Hours{
				OneHour:   1.0,
				ThreeHour: 3.0,
			},
		},
	}

	for testname, tt := range tests {
		t.Run(testname, func(t *testing.T) {
			h := weather.OWPHToH(tt.OWPHours)

			if !reflect.DeepEqual(h, tt.ExpectedHours) {
				t.Errorf("Failed to get expected hours: got \"%v\" instead of \"%v\".", h, tt.ExpectedHours)
			}
		})
	}
}
