package weather

import (
	"errors"
	"io"
	"net/http"

	"gitlab.com/PandatiX/web/api/internal"
)

// Fetcher définie l'interface pour réaliser les requêtes à l'API d'OWM.
type Fetcher interface {
	Do(*http.Request) (*http.Response, error)
}

// Client est une implémentation simple de l'interface Fetcher.
type Client struct {
	client *http.Client
}

func (c Client) Do(r *http.Request) (*http.Response, error) {
	return c.client.Do(r)
}

// DefaultClient est un Client par défaut permettant de récupérer
// du contenu en ligne.
var DefaultClient = &Client{http.DefaultClient}
var _ = (Fetcher)(&Client{})

// ErrNilFetcher est une erreur définissant que le Fetcher donné
// a pour valeur nil.
var ErrNilFetcher = errors.New("nil fetcher")

// FetchOWM appelle l'API OWM pour une ville donnée et retourne
// le contenu de cet appel.
func FetchOWM(f Fetcher, city string) ([]byte, error) {
	if f == nil {
		return nil, ErrNilFetcher
	}
	// Build and do the request to the OpenWeatherMap API
	req, err := http.NewRequest(http.MethodGet, "http://api.openweathermap.org/data/2.5/weather", nil)
	if err != nil {
		return nil, err
	}
	q := req.URL.Query()
	if Key == "" {
		internal.GetLogger().Warn("No OWP API key")
	}
	q.Add("APPID", Key)
	q.Add("units", Units)
	q.Add("lang", Lang)
	q.Add("q", city)
	req.URL.RawQuery = q.Encode()

	res, err := f.Do(req)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	// Lit le contenu de la réponse
	b, err := io.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}

	return b, nil
}
