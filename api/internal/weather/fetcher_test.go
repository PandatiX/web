package weather_test

import (
	"bytes"
	"errors"
	"io"
	"net/http"
	"testing"

	"gitlab.com/PandatiX/web/api/internal/weather"
)

var errFake = errors.New("fake error")

// FailingFetcher implémente l'interface weather.Fetcher.
// Elle retourne une erreur lors de l'appel de sa méthode Do.
type FailingFetcher struct{}

func (f FailingFetcher) Do(*http.Request) (*http.Response, error) {
	return nil, errFake
}

var _ = (weather.Fetcher)(&FailingFetcher{})

// FailingReadCloser implémente l'interface io.Reader.
// Elle retourne une erreur lors de l'appel de sa méthode Read.
type FailingReadCloser struct{}

func (f FailingReadCloser) Read([]byte) (int, error) {
	return 0, errFake
}

func (f FailingReadCloser) Close() error {
	return nil
}

var _ = (io.ReadCloser)(&FailingReadCloser{})

// FailingReaderFetcher implémente l'interface weather.Fetcher.
// Elle retourne une erreur lors de l'appel de la méthode Read sur le
// Body de la http.Response.
type FailingReaderFetcher struct{}

func (f FailingReaderFetcher) Do(*http.Request) (*http.Response, error) {
	return &http.Response{
		Body: &FailingReadCloser{},
	}, nil
}

var _ = (weather.Fetcher)(&FailingReaderFetcher{})

// FakeReadCloser implémente l'interface io.Reader.
type FakeReadCloser struct {
	data      []byte
	readIndex int64
}

// NewFakeReadCloser crée un FakeReadCloser avec comme contenu toRead.
func NewFakeReadCloser(toRead string) *FakeReadCloser {
	return &FakeReadCloser{data: []byte(toRead)}
}

func (f *FakeReadCloser) Read(p []byte) (n int, err error) {
	if f.readIndex >= int64(len(f.data)) {
		err = io.EOF
		return
	}

	n = copy(p, f.data[f.readIndex:])
	f.readIndex += int64(n)
	return
}

func (f FakeReadCloser) Close() error {
	return nil
}

var _ = (io.ReadCloser)(&FakeReadCloser{})

var payload = "{}"

// FakeFetcher implémente l'interface weather.Fetcher.
type FakeFetcher struct{}

func (f FakeFetcher) Do(*http.Request) (*http.Response, error) {
	return &http.Response{
		Body: NewFakeReadCloser(payload),
	}, nil
}

var _ = (weather.Fetcher)(&FakeFetcher{})

func TestFetchOWM(t *testing.T) {
	t.Parallel()

	var tests = map[string]struct {
		Fetcher       weather.Fetcher
		City          string
		ExpectedBytes []byte
		ExpectedErr   error
	}{
		"nil-fetcher": {
			Fetcher:       nil,
			City:          "",
			ExpectedBytes: nil,
			ExpectedErr:   weather.ErrNilFetcher,
		},
		"failing-fetcher": {
			Fetcher:       &FailingFetcher{},
			City:          "",
			ExpectedBytes: nil,
			ExpectedErr:   errFake,
		},
		"failing-reader": {
			Fetcher:       &FailingReaderFetcher{},
			City:          "",
			ExpectedBytes: nil,
			ExpectedErr:   errFake,
		},
		"valid-fetch": {
			Fetcher:       &FakeFetcher{},
			City:          "city",
			ExpectedBytes: []byte(payload),
			ExpectedErr:   nil,
		},
	}

	for testname, tt := range tests {
		t.Run(testname, func(t *testing.T) {
			b, err := weather.FetchOWM(tt.Fetcher, tt.City)

			if !bytes.Equal(b, tt.ExpectedBytes) {
				t.Errorf("Failed to get expected bytes: got \"%v\" instead of \"%v\".", b, tt.ExpectedBytes)
			}
			if err != tt.ExpectedErr {
				t.Errorf("Failed to get expected error: got \"%s\" instead of \"%s\".", err, tt.ExpectedErr)
			}
		})
	}
}
