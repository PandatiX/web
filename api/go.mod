module gitlab.com/PandatiX/web/api

go 1.16

require (
	github.com/gin-gonic/gin v1.7.1
	github.com/urfave/cli/v2 v2.3.0
	go.uber.org/zap v1.16.0
)
