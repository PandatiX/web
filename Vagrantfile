$go_setup = <<-GO_SETUP

wget -q https://golang.org/dl/go1.16.3.linux-amd64.tar.gz
tar -C /usr/local -xzf go1.16.3.linux-amd64.tar.gz
rm go1.16.3.linux-amd64.tar.gz
echo "export PATH=$PATH:/usr/local/go/bin" | sudo tee -a /etc/profile
export PATH=$PATH:/usr/local/go/bin

go version

GO_SETUP



$code_setup = <<-CODE_SETUP

sudo snap install code --classic

# Install extensions
code --extensions-dir="/home/vagrant/.vscode/extensions" --user-data-dir="/home/vagrant/.vscode/settings" --install-extension golang.go
code --extensions-dir="/home/vagrant/.vscode/extensions" --user-data-dir="/home/vagrant/.vscode/settings" --install-extension ms-azuretools.vscode-docker
code --extensions-dir="/home/vagrant/.vscode/extensions" --user-data-dir="/home/vagrant/.vscode/settings" --install-extension gitlab.gitlab-workflow

# Install go tools
export GOBIN="/home/vagrant/go/bin"
go install github.com/uudashr/gopkgs/v2/cmd/gopkgs@latest
go install github.com/ramya-rao-a/go-outline@latest
go install github.com/cweill/gotests/...@latest
go install github.com/fatih/gomodifytags@latest
go install github.com/josharian/impl@latest
go install github.com/haya14busa/goplay/cmd/goplay@latest
go install github.com/go-delve/delve/cmd/dlv@latest
go install golang.org/x/lint/golint@latest
go install golang.org/x/tools/gopls@latest

CODE_SETUP



Vagrant.configure("2") do |config|
  config.vm.box = "ubuntu/focal64"

  config.vm.network "private_network", ip: "172.28.128.5"

  config.vm.provider :virtualbox do |v|
    v.gui = true
    v.memory = 4096
    v.cpus = 4
    v.name = "ENSIBS_Web_Go_VM"
  end
  
  # Update repositories
  config.vm.provision :shell, inline: "sudo apt update -y"

  # Upgrade installed packages
  config.vm.provision :shell, inline: "sudo apt upgrade -y"
  
  # Add desktop environment
  config.vm.provision :shell, inline: "sudo apt install -y --no-install-recommends ubuntu-desktop"
  config.vm.provision :shell, inline: "sudo apt install -y --no-install-recommends virtualbox-guest-dkms virtualbox-guest-utils virtualbox-guest-x11"
  
  # Add `vagrant` to sudoers
  config.vm.provision :shell, inline: "sudo usermod -a -G sudo vagrant"
  
  # Add Firefox
  config.vm.provision :shell, inline: "sudo apt install -y --no-install-recommends firefox"
  
  # Add Go 1.16
  config.vm.provision :shell, inline: $go_setup
  
  # Add Docker
  config.vm.provision :shell, inline: "snap install docker --classic"
  config.vm.provision :shell, inline: "sudo groupadd docker && sudo usermod -aG docker vagrant"
  
  # Add VS Code with Go extensions
  config.vm.provision :shell, inline: $code_setup
  
  # Clone git repo
  config.vm.provision :shell, inline: "cd /home/vagrant && git clone https://gitlab.com/PandatiX/web.git"
  
  # Restart
  config.vm.provision :shell, inline: "sudo shutdown -r now"
end
