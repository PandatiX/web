# ENSIBS Web TP1

<img src="res/archi.png" />

Projet réalisé par:
 - Lucas VIDELAINE e2004769
 - Lucas TESSON e2004675

Le TP1 de web à l'ENSIBS comprend/utilise les technologies/outils suivantes:
 - Git
 - HTML5
 - CSS3
 - Bootstrap 4.1.3 (CSS) - 4.6.0 (JS)
 - jQuery 3.6.0
 - Popper 1.14.3
 - Gitlab
 - Docker
 - RobotFramework
 - Kubernetes
 - Golang
 - Gin
 - Zap
 - Sass
 - JSON
 - Vagrant
 - Markdown
 - OpenWeatherMap
 - WeatherIcons
 - VirtualBox
 - Prometheus
 - VSCode
 - FontAwesome
 - Helm

## Environnement de production

Afin de lancer le TP1, il vous faudra réaliser les commandes suivantes, dans
le dossier racine du projet.

```bash
minikube start
minikube addons enable ingress
helm install web chart
```

Une fois cela exécuté, il suffit de se connecter à l'URL que l'on récupère
via `kubectl get ingress` (champ "ADDRESS"). Attention, il faut attendre
quelques temps que l'adresse de l'ingress soit disponible, rien d'anormal.

## Environnement de développement

Pour lancer son environnement de développement, il suffit d'exécuter `vagrant up`
dans le dossier racine du projet. Cela va lancer une VM Ubuntu 20.04 LTS, avec Golang
dans la version du projet, VSCode et ses extensions utilisées.

Dans chaque microservice, les tests unitaires peuvent se lancer via
`go test ./... -v -cover`, et les tests fonctionnels via `cd robot && robot tests`.

Ces derniers nécessitent toutefois un petit aménagement: l'URL doit être changée
par celle de l'ingress du K8s (cf `Environnement de production`).
