module gitlab.com/PandatiX/web/front

go 1.16

require (
	github.com/prometheus/client_golang v1.10.0
	github.com/urfave/cli/v2 v2.3.0
	go.uber.org/zap v1.16.0
)
