package main

import (
	"fmt"
	"net/http"
	"os"
	"strconv"

	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/urfave/cli/v2"
	"gitlab.com/PandatiX/web/front"
	"gitlab.com/PandatiX/web/front/internal"
	"go.uber.org/zap"
)

const cliLogo = `    ______                 __
   / ____/________  ____  / /_
  / /_  / ___/ __ \/ __ \/ __/
 / __/ / /  / /_/ / / / / /_
/_/   /_/   \____/_/ /_/\__/
`

func main() {
	fmt.Println(cliLogo)

	app := &cli.App{
		Name:    "Front",
		Usage:   "CLI to manage the Front binary",
		Action:  startServer,
		Version: "v0.1.0",
		Flags: []cli.Flag{
			&cli.IntFlag{
				Name:    "port",
				Aliases: []string{"p"},
				EnvVars: []string{"PORT"},
				Usage:   "port to serves on",
				Value:   80,
			},
			&cli.StringFlag{
				Name:        "root",
				Aliases:     []string{"r"},
				Usage:       "root to refers to",
				Value:       "../",
				Destination: &front.Root,
			},
			&cli.BoolFlag{
				Name:  "no-csp",
				Usage: "disable CSP",
			},
		},
	}

	err := app.Run(os.Args)
	if err != nil {
		panic(err)
	}
}

func startServer(c *cli.Context) error {
	if c.Bool("no-csp") {
		internal.CSP = false
	}

	// Gestion du contenu statique
	http.Handle("/static/", http.StripPrefix("/static/", http.FileServer(http.Dir(front.Root+"static"))))
	http.Handle("/android-chrome-192x192.png", http.FileServer(http.FS(front.AndChr192Png)))
	http.Handle("/android-chrome-512x512.png", http.FileServer(http.FS(front.AndChr512Png)))
	http.Handle("/apple-touch-icon.png", http.FileServer(http.FS(front.AppTchPng)))
	http.Handle("/favicon-16x16.png", http.FileServer(http.FS(front.Fav16Png)))
	http.Handle("/favicon-32x32.png", http.FileServer(http.FS(front.Fav32Png)))
	http.Handle("/favicon.ico", http.FileServer(http.FS(front.FavIco)))
	http.Handle("/site.webmanifest", http.FileServer(http.FS(front.SiteWebManifest)))

	// Gestion des routes
	http.Handle("/metrics", promhttp.Handler())
	http.Handle("/contact", internal.MakeHandler(internal.ContactHandler))
	http.Handle("/help", internal.MakeHandler(internal.HelpHandler))
	http.Handle("/", internal.MakeHandler(internal.RootHandler))

	port := c.Int("port")
	internal.GetLogger().Info("Front server serving", zap.Int("port", port))
	return http.ListenAndServe(":"+strconv.Itoa(port), nil)
}
