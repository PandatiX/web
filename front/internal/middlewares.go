package internal

import (
	"fmt"
	"html/template"
	"net/http"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"gitlab.com/PandatiX/web/front"
)

// MakeHandler enveloppe un http.HandlerFunc dans les enveloppes
// globales (i.e. CSP, metrics, logs...).
func MakeHandler(f http.HandlerFunc) http.HandlerFunc {
	f = makeHeaderFooterHandler(f)
	f = makeCSPHandler(f)
	f = makeMetricsHandler(f)
	f = makeLogHandler(f)

	return f
}

// Elements en relation avec les logs
type StatusRecorder struct {
	http.ResponseWriter
	Status int
}

func (r *StatusRecorder) WriteHeader(status int) {
	r.Status = status
}

func makeLogHandler(h http.HandlerFunc) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		start := time.Now()
		recorder := &StatusRecorder{
			ResponseWriter: w,
			Status:         http.StatusOK,
		}

		h.ServeHTTP(recorder, r)

		fmt.Printf("[Front] %v | %3d | %13v | %15s | %-7s %#v\n",
			start.Format("2006/01/02 - 15:04:05"),
			recorder.Status,
			time.Since(start),
			r.RemoteAddr,
			r.Method,
			r.URL.Path)
	})
}

// Elements en relation avec les metrics
var routeMetrics = promauto.NewCounter(prometheus.CounterOpts{
	Name: "front_route_total",
	Help: "The total number of request not handeled as static content.",
})

func makeMetricsHandler(f http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		routeMetrics.Inc()

		f(w, r)
	}
}

// Elements en relation avec le CSP
var CSP bool = true

func makeCSPHandler(f http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		if CSP {
			// En mode de développement, vous devriez peut-être ajouter IP:PORT comme source autorisée dans la CSP.
			w.Header().Add("Content-Security-Policy", "default-src 'self' 'unsafe-inline' 0.0.0.0:8081 cdn.jsdelivr.net cdnjs.cloudflare.com; script-src 'self' 0.0.0.0:8081 cdnjs.cloudflare.com code.jquery.com stackpath.bootstrapcdn.com")
		}

		f(w, r)
	}
}

// Elements en relation avec les header/footer
func makeHeaderFooterHandler(f http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		template.Must(template.ParseFiles(front.Root+"template/header.gohtml")).Execute(w, nil)

		f(w, r)

		template.Must(template.ParseFiles(front.Root+"template/footer.gohtml")).Execute(w, nil)
	}
}
