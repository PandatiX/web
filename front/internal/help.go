package internal

import (
	"html/template"
	"net/http"

	"gitlab.com/PandatiX/web/front"
)

func HelpHandler(w http.ResponseWriter, r *http.Request) {
	template.Must(template.ParseFiles(front.Root+"template/help.gohtml")).Execute(w, nil)
}
