package internal

import (
	"html/template"
	"net/http"

	"gitlab.com/PandatiX/web/front"
)

func RootHandler(w http.ResponseWriter, r *http.Request) {
	template.Must(template.ParseFiles(front.Root+"template/root.gohtml")).Execute(w, nil)
}
