package internal_test

import (
	"testing"

	"gitlab.com/PandatiX/web/front/internal"
)

func TestStatusRecorderWriteHeader(t *testing.T) {
	s := &internal.StatusRecorder{}

	s.WriteHeader(1234)

	if s.Status != 1234 {
		t.Error("Failed to set status code.")
	}
}
