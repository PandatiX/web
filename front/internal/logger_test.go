package internal_test

import (
	"testing"

	"gitlab.com/PandatiX/web/front/internal"
)

func TestGetLogger(t *testing.T) {
	if internal.GetLogger() == nil {
		t.Error("Failed to get an instance of a logger.")
	}
}
