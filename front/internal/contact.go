package internal

import (
	"html/template"
	"net/http"

	"gitlab.com/PandatiX/web/front"
)

func ContactHandler(w http.ResponseWriter, r *http.Request) {
	template.Must(template.ParseFiles(front.Root+"template/contact.gohtml")).Execute(w, nil)
}
