package front

import (
	"embed"
)

var Root string

//go:embed android-chrome-192x192.png
var AndChr192Png embed.FS

//go:embed android-chrome-512x512.png
var AndChr512Png embed.FS

//go:embed apple-touch-icon.png
var AppTchPng embed.FS

//go:embed favicon-16x16.png
var Fav16Png embed.FS

//go:embed favicon-32x32.png
var Fav32Png embed.FS

//go:embed favicon.ico
var FavIco embed.FS

//go:embed site.webmanifest
var SiteWebManifest embed.FS
