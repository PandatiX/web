#!/bin/bash

# Récupère les fichiers .scss
cd scss
files=$(find . | grep \\.scss$)
cd ..

# Compile les .css pour chaque .scss
for f in $files
do
    dest="static/stylesheets/${f%.scss}.css"
    echo "Compiling $f into $dest"
    sass --no-source-map --style compressed "scss/$f" $dest
done
