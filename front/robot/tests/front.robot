*** Settings ***
Documentation  Test suite for the front.
Library        Browser

Suite Setup     New Page  http://192.168.49.2
Suite Teardown  Close Page

*** Test Cases ***
Root Endpoint
    [Documentation]  This test ensures the / endpoint is reachable.

    &{response}=     HTTP                /       GET
    Should Be Equal  ${response.status}  ${200}

Contact Endpoint
    [Documentation]  This test ensures the /contact endpoint is reachable.

    &{response}=     HTTP                /contact  GET
    Should Be Equal  ${response.status}  ${200}

Help Endpoint
    [Documentation]  This test ensures the /help endpoint is reachable.

    &{response}=     HTTP                /help   GET
    Should Be Equal  ${response.status}  ${200}

Metrics Endpoint
    [Documentation]  This test ensures the /metrics endpoint is reachable.

    &{response}=     HTTP                /metrics  GET
    Should Be Equal  ${response.status}  ${200}

Static Dir
    [Documentation]  This test ensures the static files are reachable under
    ...              /static endpoint.

    &{response}=     HTTP                /static  GET
    Should Be Equal  ${response.status}  ${200}

Unexisting Static File
    [Documentation]  This test ensures static files that does not exists
    ...              are not reachable, with a 404 Not Found status.

    &{response}=     HTTP                /static/unexisting.txt  GET
    Should Be Equal  ${response.status}  ${404}

Check CSP Header On Non Static Endpoint
    [Documentation]  This test ensures non-static endpoints have a CSP-header.

    &{response}=         HTTP                                            /  GET
    Should Not Be Empty  ${response.headers['content-security-policy']}

Weather For A City
    [Documentation]  This test ensures the UI works as expected to look for a city.

    Type Text      xpath=//*[@id="city"]        Brest
    Click          xpath=//*[@id="btn-search"]
    Sleep          5s
    Get Classes    xpath=//*[@id="city"]        ==     form-control
    Get Attribute  xpath=//*[@id="results"]     style  !=            display: none;
