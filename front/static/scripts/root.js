$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();

    $("#btn-search").click(function(){
        $("#city").removeClass("is-invalid");
        $("#results").hide();
        $("#lds").show();

        // Query the API
        $.get("/api/weather/" + $("#city").val(), function(data){
            $("#lds").hide();
            $("#results").show();

            /***** GLOBAL CARD *****/
            // TODO #weather_icon

            $("#weather_main").text(data.weather.main);

            $("#main_temp").text(data.main.temp + " °C");
            $("#main_feels_like").text(data.main.feels_like + " °C");
            $("#main_temp_min").text(data.main.temp_min + " °C");
            $("#main_temp_max").text(data.main.temp_max + " °C");

            $("#main_pressure").text(data.main.pressure + " hPa");
            $("#main_humidity").text(data.main.humidity + " %");
            if (data.main.sea_level != null) {
                $("#main_sea_level").text(data.main.sea_level + " mm");
                $("#wmsl").show();
            } else {
                $("#wmsl").hide();
            }
            if (data.main.grnd_level != null) {
                $("#main_grnd_level").text(data.main.grnd_level + " mm");
                $("#wmgl").show();
            } else {
                $("#wmgl").hide();
            }

            $("#weather_description").text(data.weather.description);

            /***** WIND CARD *****/
            $("#wind_speed").text(data.wind.speed + " m/s");
            $("#wind_deg").text(data.wind.deg + " °");
            $("#compass").css({ transform: "rotate("+(data.wind.deg - 45)+"deg) "});
            if (data.wind.gust != null) {
                $("#wind_gust").text(data.wind.gust + " m/s²");
                $("#wwg").show();
            } else {
                $("#wwg").hide();
            }

            /***** RAIN CARD *****/
            if (data.rain != null) {
                $("#rain_1h").text(data.rain.one_h + " mm");
                $("#rain_3h").text(data.rain.three_h + " mm");
                $("#card-rain").show();
            } else {
                $("#card-rain").hide();
            }

            /***** SNOW CARD *****/
            if (data.snow != null) {
                $("#snow_1h").text(data.snow.one_h + " mm");
                $("#snow_3h").text(data.snow.three_h + " mm");
                $("#card-snow").show();
            } else {
                $("#card-snow").hide();
            }
        })
          .fail(function(){
            $("#lds").hide();
            $("#city").addClass("is-invalid");
        });
    });
});
